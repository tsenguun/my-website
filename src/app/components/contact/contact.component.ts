import { Component, OnInit } from '@angular/core';
import { ImessageService } from '../../services/imessage.service';
import { User } from '../../models/contact_info';
import { Router } from '@angular/router';
// import { FlashMessagesService } from 'ngx-flash-messages';
import { FlashMessagesService } from 'angular2-flash-messages';
import {HttpClient} from '@angular/common/http';

export interface FormModel {
  captcha?: string;
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  user: User = {
    name: '',
    email: '',
    message: ''
  };
  private emailUrl = '/assets/email.php';
  public formModel: FormModel = {};

  constructor(
    private flashmsg: FlashMessagesService,
    private router: Router,
    private appService: ImessageService,
    private http: HttpClient
  ) { }


  sendEmail({value, valid}: {value: User, valid: boolean}) {
    if (valid) {
      this.http.post(this.emailUrl, value)
        .subscribe(response => {
            this.emailSent();
          },
          error => {
            const status = error['status'];
            if (status === 200) {
              this.emailSent();
            } else {
              this.emailNotSent();
            }
          });
    } else {
      this.flashmsg.show('Please fill in all fields', {
        cssClass: 'alert-danger', timeout: 4000
      });
      this.router.navigate(['contact']);
    }
  }

  emailSent() {
    this.flashmsg.show('Email sent!', {
      cssClass: 'alert-success', timeout: 4000
    });
    this.router.navigate(['contact']);
  }

  emailNotSent() {
    this.flashmsg.show('Email did not go through! Send To: tsenguun@uw.edu', {
      cssClass: 'alert-danger', timeout: 4000
    });
    this.router.navigate(['contact']);
  }

  //
  // sendEmail({value, valid}: {value: User, valid: boolean}) {
  //   if (valid) {
  //     const response = this.appService.sendEmail(value);
  //     console.log('Here: ', response);
  //     if (response === true) {
  //       // console.log('App Component Success', res);
  //       console.log('App Component Success');
  //       this.flashmsg.show('Email sent!', {
  //         cssClass: 'alert-success', timeout: 4000
  //       });
  //       this.router.navigate(['contact']);
  //     } else {
  //       this.flashmsg.show('Email did not go through! Send To: tsenguun@uw.edu', {
  //         cssClass: 'alert-danger', timeout: 4000
  //       });
  //       this.router.navigate(['contact']);
  //     }
  //   } else {
  //     this.flashmsg.show('Please fill in all fields', {
  //       cssClass: 'alert-danger', timeout: 4000
  //     });
  //     this.router.navigate(['contact']);
  //   }
  // }

  ngOnInit() {
  }

}
