import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-db',
  templateUrl: './student-db.component.html',
  styleUrls: ['./student-db.component.scss']
})
export class StudentDbComponent implements OnInit {
  myInterval = 2500;
  slides: any[] = [];
  activeSlideIndex = 0;
  noWrapSlides = false;
  title = [
    'Add student data',
    'Data Display',
    'Check For Duplicates',
    'File open mechanic',
    'Search data tab',
    'Sample error message',
    'Database Layout'
  ];
  file_name = ['Add_data', 'ViewTable', 'check_duplicate', 'File_open', 'Search_tab', 'Tab2_max_error', 'Database'];
  constructor() {
    for (let i = 0; i < this.title.length; i++) {
      this.addSlide(i);
    }
  }

  addSlide(i): void {
    this.slides.push({
      image: `assets/img/project/StudentDatabase/${this.file_name[i]}.PNG`,
      text: this.title[i]
    });
  }

  ngOnInit() {
  }

}
