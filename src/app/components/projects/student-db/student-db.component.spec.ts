import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentDbComponent } from './student-db.component';

describe('StudentDbComponent', () => {
  let component: StudentDbComponent;
  let fixture: ComponentFixture<StudentDbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentDbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentDbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
