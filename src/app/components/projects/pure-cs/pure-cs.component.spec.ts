import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureCsComponent } from './pure-cs.component';

describe('PureCsComponent', () => {
  let component: PureCsComponent;
  let fixture: ComponentFixture<PureCsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureCsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureCsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
