import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-magic',
  templateUrl: './magic.component.html',
  styleUrls: ['./magic.component.scss']
})
export class MagicComponent implements OnInit {
  myInterval = 2500;
  slides: any[] = [];
  activeSlideIndex = 0;
  noWrapSlides = false;
  title = ['Before Reveal', 'After Reveal'];
  constructor() {
    for (let i = 0; i < this.title.length; i++) {
      this.addSlide(i);
    }
  }

  addSlide(i): void {
    this.slides.push({
      image: `assets/img/project/magic/${i + 1}.PNG`,
      text: this.title[i]
    });
  }
  ngOnInit() {
  }

}
