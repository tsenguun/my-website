import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Project570Component } from './project-570.component';

describe('Project570Component', () => {
  let component: Project570Component;
  let fixture: ComponentFixture<Project570Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Project570Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Project570Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
