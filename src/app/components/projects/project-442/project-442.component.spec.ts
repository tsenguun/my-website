import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Project442Component } from './project-442.component';

describe('Project442Component', () => {
  let component: Project442Component;
  let fixture: ComponentFixture<Project442Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Project442Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Project442Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
