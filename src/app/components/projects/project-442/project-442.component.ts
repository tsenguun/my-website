import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-project-442',
  templateUrl: './project-442.component.html',
  styleUrls: ['./project-442.component.scss']
})
export class Project442Component implements OnInit {
  myInterval = 2500;
  slides: any[] = [];
  activeSlideIndex = 0;
  noWrapSlides = false;
  file_name = [
    '1',
    '5',
    '2',
    '3',
    '4'
  ];
  title = [
    'Building View - 1',
    'Building View - 2',
    'Plan View',
    'Beam Cross Sections',
    'Column and Beam moment distribution'
  ];
  constructor() {
    for (let i = 0; i < this.title.length; i++) {
      this.addSlide(i);
    }
  }

  addSlide(i): void {
    this.slides.push({
      image: `assets/img/project/442/${this.file_name[i]}.png`,
      text: this.title[i]
    });
  }

  ngOnInit() {
  }

}
