import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrimsonCode2018Component } from './crimson-code-2018.component';

describe('CrimsonCode2018Component', () => {
  let component: CrimsonCode2018Component;
  let fixture: ComponentFixture<CrimsonCode2018Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrimsonCode2018Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrimsonCode2018Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
