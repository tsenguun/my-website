import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-crimson-code-2018',
  templateUrl: './crimson-code-2018.component.html',
  styleUrls: ['./crimson-code-2018.component.scss']
})
export class CrimsonCode2018Component implements OnInit {
  myInterval = 2500;
  slides: any[] = [];
  activeSlideIndex = 0;
  noWrapSlides = false;
  title = ['Web app - input', 'Web app - output', 'Mobile app - input', 'Mobile app - output'];

  constructor() {
    for (let i = 1; i <= this.title.length; i++) {
      this.addSlide(i);
    }
  }

  addSlide(i): void {
    this.slides.push({
      image: `assets/img/project/crimsoncode2018/${i}.JPG`,
      text: this.title[i - 1]
    });
  }

  ngOnInit() {
  }


}
