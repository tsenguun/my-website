import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConcreteSectionWinComponent } from './concrete-section-win.component';

describe('ConcreteSectionWinComponent', () => {
  let component: ConcreteSectionWinComponent;
  let fixture: ComponentFixture<ConcreteSectionWinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConcreteSectionWinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcreteSectionWinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
