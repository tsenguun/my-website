import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-concrete-section-win',
  templateUrl: './concrete-section-win.component.html',
  styleUrls: ['./concrete-section-win.component.scss']
})
export class ConcreteSectionWinComponent implements OnInit {
  myInterval = 2500;
  slides: any[] = [];
  activeSlideIndex = 0;
  noWrapSlides = false;
  title = [
    'Specify the concrete layers', 'Specify the steel layers', 'Example 1: Realistic I beam',
    'Example 2: Custom christmas tree'
  ];
  constructor() {
    for (let i = 0; i < this.title.length; i++) {
      this.addSlide(i);
    }
  }

  addSlide(i): void {
    this.slides.push({
      image: `assets/img/project/SectionAnalysis/${i + 1}.PNG`,
      text: this.title[i]
    });
  }

  ngOnInit() {
  }

}
