import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-project-506',
  templateUrl: './project-506.component.html',
  styleUrls: ['./project-506.component.scss']
})
export class Project506Component implements OnInit {
  myInterval = 2500;
  slides: any[] = [];
  activeSlideIndex = 0;
  noWrapSlides = false;
  title = [
    'Stress field in compression test', 'Stress field of steel',
    'Comparison plot'
  ];
  file_name = ['stress', 'bar_stress', 'plot'];
  constructor() {
    for (let i = 0; i < this.title.length; i++) {
      this.addSlide(i);
    }
  }

  addSlide(i): void {
    this.slides.push({
      image: `assets/img/project/506/${this.file_name[i]}.png`,
      text: this.title[i]
    });
  }

  ngOnInit() {
  }

}
