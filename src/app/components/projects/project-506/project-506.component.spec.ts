import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Project506Component } from './project-506.component';

describe('Project506Component', () => {
  let component: Project506Component;
  let fixture: ComponentFixture<Project506Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Project506Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Project506Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
