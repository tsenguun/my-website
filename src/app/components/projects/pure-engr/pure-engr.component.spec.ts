import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PureEngrComponent } from './pure-engr.component';

describe('PureEngrComponent', () => {
  let component: PureEngrComponent;
  let fixture: ComponentFixture<PureEngrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PureEngrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PureEngrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
