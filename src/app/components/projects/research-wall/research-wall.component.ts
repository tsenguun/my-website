import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-research-wall',
  templateUrl: './research-wall.component.html',
  styleUrls: ['./research-wall.component.scss']
})
export class ResearchWallComponent implements OnInit {
  myInterval = 2500;
  slides: any[] = [];
  activeSlideIndex = 0;
  noWrapSlides = false;
  title = [
    'Push Over Analysis - Comparison', 'Incremental Dynamic Analysis', 'Comparison between measured and buckling models',
    'Different Integration plots', 'Stress Strain of a given section'
  ];
  file_name = ['pushover', 'IDA', 'comparison', 'IP', 'StressStrain'];
  constructor() {
    for (let i = 0; i < this.title.length; i++) {
      this.addSlide(i);
    }
  }

  addSlide(i): void {
    this.slides.push({
      image: `assets/img/project/ResearchWall/${this.file_name[i]}.JPG`,
      text: this.title[i]
    });
  }

  ngOnInit() {
  }

}
