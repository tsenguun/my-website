import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchWallComponent } from './research-wall.component';

describe('ResearchWallComponent', () => {
  let component: ResearchWallComponent;
  let fixture: ComponentFixture<ResearchWallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResearchWallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchWallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
