import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlphaBetaComponent } from './alpha-beta.component';

describe('AlphaBetaComponent', () => {
  let component: AlphaBetaComponent;
  let fixture: ComponentFixture<AlphaBetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlphaBetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlphaBetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
