import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alpha-beta',
  templateUrl: './alpha-beta.component.html',
  styleUrls: ['./alpha-beta.component.scss']
})
export class AlphaBetaComponent implements OnInit {
  myInterval = 2500;
  slides: any[] = [];
  activeSlideIndex = 0;
  noWrapSlides = false;
  title = ['Main Menu', 'How To Play', 'Sample Game', 'Sample Game', 'High Score Table'];
  file_name = ['mainMenu', 'howToPlay', 'Start1', 'Start2', 'highScore'];
  constructor() {
    for (let i = 0; i < this.file_name.length; i++) {
      this.addSlide(i);
    }
  }

  addSlide(i): void {
    this.slides.push({
      image: `assets/img/project/AlphaBeta/${this.file_name[i]}.JPG`,
      text: this.title[i]
    });
  }

  ngOnInit() {
  }

}
