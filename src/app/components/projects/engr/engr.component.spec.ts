import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngrComponent } from './engr.component';

describe('EngrComponent', () => {
  let component: EngrComponent;
  let fixture: ComponentFixture<EngrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
