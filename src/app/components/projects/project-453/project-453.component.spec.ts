import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Project453Component } from './project-453.component';

describe('Project453Component', () => {
  let component: Project453Component;
  let fixture: ComponentFixture<Project453Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Project453Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Project453Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
