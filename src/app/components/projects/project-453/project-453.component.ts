import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-project-453',
  templateUrl: './project-453.component.html',
  styleUrls: ['./project-453.component.scss']
})
export class Project453Component implements OnInit {
  myInterval = 2500;
  slides: any[] = [];
  activeSlideIndex = 0;
  noWrapSlides = false;
  title = [
    'Foundation Cross Section',
    'Foundation Cross Section after filling',
    'Column Installation',
    'Cap Beam Install',
    'Side view',
    'Girder Install',
    'Girder Image',
    'Drop in span',
    'Concrete Fill',
    'Completed Concrete Fill',
    'Final Result'
  ];
  file_name = ['1-Foundation%20Cross%20Section',
    '2-Foundation%20Cross%20Section%20after%20Fill',
    '3-Column%20installation',
    '4-Cap%20beam%20install',
    '5-Desired',
    '6-Girder%20Intall',
    '7-Close%20up%20image%20-%20Girder',
    '8%20-%20Drop%20in%20span',
    '9%20-%20Concrete%20Fill',
    '10%20-%20Concrete%20Fill%20-%20Complete',
    '11%20-%20Final%20Image'
  ];
  constructor() {
    for (let i = 0; i < this.title.length; i++) {
      this.addSlide(i);
    }
  }

  addSlide(i): void {
    this.slides.push({
      image: `assets/img/project/453/${this.file_name[i]}.JPG`,
      text: this.title[i]
    });
  }


  ngOnInit() {
  }

}
