import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-concrete-section-web',
  templateUrl: './concrete-section-web.component.html',
  styleUrls: ['./concrete-section-web.component.scss']
})
export class ConcreteSectionWebComponent implements OnInit {
  myInterval = 2500;
  slides: any[] = [];
  activeSlideIndex = 0;
  noWrapSlides = false;
  title = ['Home Page', 'Rectangular Section', 'Material/Output', 'T-Beam', 'Custom Section - Mongolian - SI unit', 'History'];

  constructor() {
    for (let i = 1; i <= 6; i++) {
      this.addSlide(i);
    }
  }

  addSlide(i): void {
    this.slides.push({
      image: `assets/img/project/beamCalculator/${i}.JPG`,
      text: this.title[i - 1]
    });
  }

  ngOnInit() {
  }

}
