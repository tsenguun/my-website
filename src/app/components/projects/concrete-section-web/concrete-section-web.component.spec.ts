import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConcreteSectionWebComponent } from './concrete-section-web.component';

describe('ConcreteSectionWebComponent', () => {
  let component: ConcreteSectionWebComponent;
  let fixture: ComponentFixture<ConcreteSectionWebComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConcreteSectionWebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcreteSectionWebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
