import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherEngrComponent } from './other-engr.component';

describe('OtherEngrComponent', () => {
  let component: OtherEngrComponent;
  let fixture: ComponentFixture<OtherEngrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherEngrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherEngrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
