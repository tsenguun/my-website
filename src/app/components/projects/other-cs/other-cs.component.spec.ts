import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherCsComponent } from './other-cs.component';

describe('OtherCsComponent', () => {
  let component: OtherCsComponent;
  let fixture: ComponentFixture<OtherCsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherCsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherCsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
