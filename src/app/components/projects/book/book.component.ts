import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {
  myInterval = 2500;
  slides: any[] = [];
  activeSlideIndex = 0;
  noWrapSlides = false;
  title = ['Cover', 'Published', 'Giveaway recipient'];
  constructor() {
    for (let i = 0; i < this.title.length; i++) {
      this.addSlide(i);
    }
  }

  addSlide(i): void {
    this.slides.push({
      image: `assets/img/project/book/${i + 1}.jpg`,
      text: this.title[i]
    });
  }


  ngOnInit() {
  }

}
