import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsEngrComponent } from './cs-engr.component';

describe('CsEngrComponent', () => {
  let component: CsEngrComponent;
  let fixture: ComponentFixture<CsEngrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsEngrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsEngrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
