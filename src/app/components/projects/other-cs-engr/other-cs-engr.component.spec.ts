import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherCsEngrComponent } from './other-cs-engr.component';

describe('OtherCsEngrComponent', () => {
  let component: OtherCsEngrComponent;
  let fixture: ComponentFixture<OtherCsEngrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherCsEngrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherCsEngrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
