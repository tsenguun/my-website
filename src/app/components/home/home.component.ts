import { Component, OnInit } from '@angular/core';
import { NgsRevealConfig } from 'ng2-scrollreveal/ngs-reveal-config';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private config: NgsRevealConfig) {
    config.duration = 1000;
    config.easing = 'cubic-bezier(0.645, 0.045, 0.355, 1)';
  }

  ngOnInit() {
  }

}
