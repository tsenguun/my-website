import { Component } from '@angular/core';
import { routerAnimation} from './router.animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ routerAnimation ]
})
export class AppComponent {
  title = 'app';

  getRouteAnimation(outlet) {
    return outlet.activatedRouteData.animation;
  }
}
