///<reference path='components/home/home.component.ts'/>
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {AboutComponent} from './components/about/about.component';
import {EducationComponent} from './components/education/education.component';
import {WorkComponent} from './components/work/work.component';
import {MainComponent} from './components/projects/main/main.component';
import {ContactComponent} from './components/contact/contact.component';
import {BookComponent} from './components/projects/book/book.component';
import {ReferenceComponent} from './components/reference/reference.component';
import {CsComponent} from './components/projects/cs/cs.component';
import {AlphaBetaComponent} from './components/projects/alpha-beta/alpha-beta.component';
import {ConcreteSectionWebComponent} from './components/projects/concrete-section-web/concrete-section-web.component';
import {MyWebsiteComponent} from './components/projects/my-website/my-website.component';
import {MagicComponent} from './components/projects/magic/magic.component';
import {Project442Component} from './components/projects/project-442/project-442.component';
import {Project453Component} from './components/projects/project-453/project-453.component';
import {Project506Component} from './components/projects/project-506/project-506.component';
import {ConcreteSectionWinComponent} from './components/projects/concrete-section-win/concrete-section-win.component';
import {ResearchWallComponent} from './components/projects/research-wall/research-wall.component';
import {EngrComponent} from './components/projects/engr/engr.component';
import {StudentDbComponent} from './components/projects/student-db/student-db.component';
import {CrimsonCode2018Component} from './components/projects/crimson-code-2018/crimson-code-2018.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'about', component: AboutComponent, data: {animation: 'about'}},
  {path: 'education', component: EducationComponent, data: {animation: 'education'}},
  {path: 'work', component: WorkComponent, data: {animation: 'work'}},
  {path: 'reference', component: ReferenceComponent, data: {animation: 'reference'}},
  {path: 'projects', component: MainComponent, data: {animation: 'main'}},
  {path: 'contact', component: ContactComponent, data: {animation: 'contact'}},
  {path: 'projects/book', component: BookComponent, data: {animation: 'book'}},

  {path: 'projects/cs', component: CsComponent, data: {animation: 'cs'}},
  {path: 'projects/cs/alpha-beta', component: AlphaBetaComponent, data: {animation: 'alphabeta'}},
  {path: 'projects/cs/crimsoncode-2018', component: CrimsonCode2018Component, data: {animation: 'crimsoncode-2018'}},
  {path: 'projects/cs/concrete-section-web', component: ConcreteSectionWebComponent, data: {animation: 'concreteSectionWeb'}},
  {path: 'projects/cs/my-website', component: MyWebsiteComponent, data: {animation: 'myWebsite'}},
  {path: 'projects/cs/student-db', component: StudentDbComponent, data: {animation: 'studentDb'}},
  {path: 'projects/cs/magic', component: MagicComponent, data: {animation: 'magic'}},

  {path: 'projects/engr', component: EngrComponent, data: {animation: 'engr'}},
  {path: 'projects/engr/project-442', component: Project442Component, data: {animation: 'project442'}},
  {path: 'projects/engr/project-453', component: Project453Component, data: {animation: 'project453'}},
  {path: 'projects/engr/project-506', component: Project506Component, data: {animation: 'project506'}},

  {path: 'projects/cse/concrete-section-win', component: ConcreteSectionWinComponent, data: {animation: 'concreteSectionWin'}},
  {path: 'projects/cse/project-442', component: Project442Component, data: {animation: 'project442'}},
  {path: 'projects/cse/research-wall', component: ResearchWallComponent, data: {animation: 'researchWall'}},
  {path: '**', component: HomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
