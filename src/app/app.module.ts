import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgsRevealModule } from 'ng2-scrollreveal';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { FlashMessagesModule } from 'ngx-flash-messages';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { HttpClientModule } from '@angular/common/http';
import { CollapseModule } from 'ngx-bootstrap';
import { CarouselModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { EducationComponent } from './components/education/education.component';
import { ReferenceComponent } from './components/reference/reference.component';
import { AlphaBetaComponent } from './components/projects/alpha-beta/alpha-beta.component';
import { BookComponent } from './components/projects/book/book.component';
import { ConcreteSectionWebComponent } from './components/projects/concrete-section-web/concrete-section-web.component';
import { ConcreteSectionWinComponent } from './components/projects/concrete-section-win/concrete-section-win.component';
import { CsComponent } from './components/projects/cs/cs.component';
import { CsEngrComponent } from './components/projects/cs-engr/cs-engr.component';
import { EngrComponent } from './components/projects/engr/engr.component';
import { MagicComponent } from './components/projects/magic/magic.component';
import { MainComponent } from './components/projects/main/main.component';
import { MyWebsiteComponent } from './components/projects/my-website/my-website.component';
import { OtherCsComponent } from './components/projects/other-cs/other-cs.component';
import { OtherCsEngrComponent } from './components/projects/other-cs-engr/other-cs-engr.component';
import { OtherEngrComponent } from './components/projects/other-engr/other-engr.component';
import { Project442Component } from './components/projects/project-442/project-442.component';
import { Project453Component } from './components/projects/project-453/project-453.component';
import { Project506Component } from './components/projects/project-506/project-506.component';
import { PureCsComponent } from './components/projects/pure-cs/pure-cs.component';
import { PureEngrComponent } from './components/projects/pure-engr/pure-engr.component';
import { ResearchWallComponent } from './components/projects/research-wall/research-wall.component';
import { StudentDbComponent } from './components/projects/student-db/student-db.component';
import {ImessageService} from './services/imessage.service';
import { WorkComponent } from './components/work/work.component';
import { CrimsonCode2018Component } from './components/projects/crimson-code-2018/crimson-code-2018.component';
import { Project570Component } from './components/projects/project-570/project-570.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    FooterComponent,
    AboutComponent,
    ContactComponent,
    EducationComponent,
    ReferenceComponent,
    AlphaBetaComponent,
    BookComponent,
    ConcreteSectionWebComponent,
    ConcreteSectionWinComponent,
    CsComponent,
    CsEngrComponent,
    EngrComponent,
    MagicComponent,
    MainComponent,
    MyWebsiteComponent,
    OtherCsComponent,
    OtherCsEngrComponent,
    OtherEngrComponent,
    Project442Component,
    Project453Component,
    Project506Component,
    PureCsComponent,
    PureEngrComponent,
    ResearchWallComponent,
    StudentDbComponent,
    WorkComponent,
    CrimsonCode2018Component,
    Project570Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgsRevealModule.forRoot(),
    FlashMessagesModule.forRoot(),
    FormsModule,
    RecaptchaModule.forRoot(),
    HttpClientModule,
    RecaptchaFormsModule,
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    CarouselModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [ImessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
