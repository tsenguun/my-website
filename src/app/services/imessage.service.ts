import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User } from '../models/contact_info';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ImessageService {
  private emailUrl = '/assets/email.php';
  constructor(private http: HttpClient) {}



  // sendEmail(message: User): Observable<User> | any {
  //   return this.http.post(this.emailUrl, message)
  //     .subscribe(response => {
  //       console.log('Sending Email was successful!');
  //       return Observable.of(response);
  //     },
  //       msg => {
  //         console.log('Sending email got Error!');
  //         return Observable.throw(msg);
  //       });
  // }



  sendEmail(message: User) {
    return this.http.post(this.emailUrl, message)
      .subscribe(response => {
        // console.log('Sending Email was successful!', response);
        console.log('Sending Email was successful!', response);
        return response;
      },
        error => {
          console.log('Sending email got Error!', error);
          return error;
        });
      }
}


